package com.koushik.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher r=req.getRequestDispatcher("views/login.jsp");
		r.forward(req, resp);
	}

	private boolean checkLogin(String email,String pass){
		return email.equals("admin@mail") && pass.equals("admin");
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		PrintWriter out=resp.getWriter();
		resp.setContentType("text/html");
		if(checkLogin(req.getParameter("email"),req.getParameter("password")))
			out.println("Login Success");
		else
			out.println("Login Failed");
		out.close();
	}
}
