package com.koushik.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class StarterServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		/*PrintWriter out=resp.getWriter();
		resp.setContentType("text/html");
		out.println("Super Servlet");
		out.close();*/
		RequestDispatcher r=req.getRequestDispatcher("views/home.jsp");
		r.forward(req, resp);
	}

}
