package com.koushik.jetty;

import org.eclipse.jetty.annotations.AnnotationConfiguration;
import org.eclipse.jetty.plus.webapp.EnvConfiguration;
import org.eclipse.jetty.plus.webapp.PlusConfiguration;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.Configuration;
import org.eclipse.jetty.webapp.FragmentConfiguration;
import org.eclipse.jetty.webapp.MetaInfConfiguration;
import org.eclipse.jetty.webapp.WebAppContext;
import org.eclipse.jetty.webapp.WebInfConfiguration;
import org.eclipse.jetty.webapp.WebXmlConfiguration;

public class StandAloneServer {
	public static void main(String args[]) throws Exception{
		Server server =new Server(8080);
		WebAppContext context=new WebAppContext();
		context.setResourceBase("src/main/webapp");
		context.setContextPath("/koushik");
		//context.setDescriptor("src/main/webapp/WEB-INF/web.xml");
		context.setAttribute("org.eclipse.jetty.server.webapp.ContainerIncludeJarPattern",".*/[^/]*jstl.*\\.jar$");
		
		context.setConfigurations(new Configuration[] {
	            new AnnotationConfiguration(),
	            new WebXmlConfiguration(),
	            new WebInfConfiguration(),
	            new PlusConfiguration(), 
	            new MetaInfConfiguration(),
	            new FragmentConfiguration(), 
	            new EnvConfiguration() });  
		
		server.setHandler(context);
		server.start();
		System.out.println("Jetty Started: localhost:8080/koushik");
		server.join();
	}
}
