# Simple Servlet JSP 

### Step by Step

1. Create maven project (will have error in pom-&gt; no web.xml)
2. Right Click project in package explorer, select 'JavaEE Tools' -&gt; 'Generate Deployment Descriptor Stub' (will generate web.xml)
3. In pom.xml, add dependency javax.servlet-api
4. Add a servlet class 'StarterServlet' in 'src/main/java' and extend it from HttpServlet
6. Put code there ... (GET, POST, ...)
7. In web.xml, add servlet,servlet-name,servlet-class mentioning StarterServlet and also add a servlet-mapping,servlet-name(StarterServlet),url-pattern(/)
8. In webapp folder(src/main/webapp) create jsp file
9. For static resources, create 'resources' folder and add a servlet mapping before any other servlet mappings, servlet-name(default),url-pattern(/resources/*)
9. In jsp page, add link to static resources like &quot;/resources/xyz&quot;
10. Works


# Run with Jetty

```
mvn compile

mvn exec:java -Dexec.mainClass="com.koushik.jetty.StandAloneServer"
```
